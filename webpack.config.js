var webpack = require('webpack');
import path from "path";
const VueLoaderPlugin = require('vue-loader/lib/plugin')

var argv = require('yargs').argv,
    isProduction = (argv.production === undefined) ? false : true;

module.exports = {
    mode: isProduction ? 'production' : 'development',
    entry: {
        'main': path.resolve(__dirname, 'src/js/main.js'),
        'main-html': path.resolve(__dirname, 'src/js/main-html.js'),
    },
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },
    module: {
        rules: [{
            test: /\.(js|jsx)$/,
            exclude: /(node_modules)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            }
        }, 
        {
            test: /\.vue$/,
            loader: 'vue-loader'
        }
    
        ],
    },
    plugins: [
        new webpack.ProvidePlugin({
            axios: 'axios',
            Vue: 'vue'
        }),
        new VueLoaderPlugin()
    ],
    resolve: {
        extensions: ['.js', '.vue', '.json'],
        alias: {
            'vue$'  : 'vue/dist/vue.esm.js',
            '@c'    : path.resolve('src/js/components/'),
            '@scss' : path.resolve('src/scss/'),
            '@js'   : path.resolve('src/js/')
        }
    },
};