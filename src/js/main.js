import Vue from "vue";
window.Vue = require('vue');
import popup from "./modules/popups";
import enquire from "enquire.js";
import responsive from "./utils/responsive";
import _t from "./utils/translate";
import mobileNav from "./modules/mobile-nav";
import merge from "lodash-es/merge";

Object.defineProperty(Vue.prototype, '_trans', { value: _t });
var env = {
    'WP_API_URL': 'https://neo.dev.va-promotion.ru/wp-json/va/v1/builder/'
};

window.env = typeof (window.env) != 'undefined' ? merge(env, window.env) : env;
Vue.component('section-title',      require("@c/ui/SectionTitle.vue").default);
Vue.component('btn',      			require("@c/ui/Button.vue").default);

Vue.component('card-media',			require("@c/inc/CardMedia.vue").default);
Vue.component('card-advantage',		require("@c/inc/CardAdvantage.vue").default);
Vue.component('card-feedback',		require("@c/inc/CardFeedback.vue").default);
Vue.component('card-timeline',		require("@c/inc/CardTimeline.vue").default);
Vue.component('card-coach',			require("@c/inc/CardCoach.vue").default);
Vue.component('card-event',         require("@c/inc/CardEvent.vue").default);
Vue.component('card-tarife',        require("@c/inc/CardTarife.vue").default);
Vue.component('card-note',        require("@c/inc/CardNote.vue").default);
Vue.component('card-blog-post',		require("@c/inc/CardBlogPost.vue").default);
Vue.component('select-city',        require("@c/inc/SelectCity.vue").default);
Vue.component('banner-content',     require("@c/inc/BannerContent.vue").default);

Vue.component('builder',			require("@c/Builder.vue").default);

Vue.component('block',				require("@c/blocks/Block.vue").default);
Vue.component('block-intro',		require("@c/blocks/BlockIntro.vue").default);
Vue.component('block-intro-slider',	require("@c/blocks/BlockIntroSlider.vue").default);
Vue.component('block-ti',        	require("@c/blocks/BlockTextImage.vue").default);
Vue.component('block-tarife',       require("@c/blocks/BlockTarife.vue").default);
Vue.component('block-media',		require("@c/blocks/BlockMedia.vue").default);
Vue.component('block-advantage',	require("@c/blocks/BlockAdvantage.vue").default);
Vue.component('block-consultation', require("@c/blocks/BlockConsultation.vue").default);
Vue.component('block-feedbacks',	require("@c/blocks/BlockFeedbacks.vue").default);
Vue.component('page-feedbacks',     require("@c/blocks/PageFeedbacks.vue").default);
Vue.component('block-timeline',     require("@c/blocks/BlockTimeline.vue").default);
Vue.component('block-coach',		require("@c/blocks/BlockCoaches.vue").default);
Vue.component('block-blog-posts',	require("@c/blocks/BlockBlogPosts.vue").default);
Vue.component('block-blog-posts-builder',	require("@c/blocks/BlockBlogPostsBuilder.vue").default);
Vue.component('banner',             require("@c/blocks/Banner.vue").default);
Vue.component('block-map',          require("@c/blocks/Map.vue").default);
Vue.component('page-events',        require("@c/blocks/PageEvents.vue").default);
Vue.component('block-event',        require("@c/blocks/BlockEvent.vue").default);
Vue.component('block-notes',        require("@c/blocks/BlockNotes.vue").default);
Vue.component('block-comments',     require("@c/blocks/BlockComments.vue").default);
Vue.component('block-form-callback',require("@c/blocks/BlockFormCallback.vue").default);
Vue.component('block-form-feedback',require("@c/blocks/BlockFormFeedback.vue").default);

Vue.component('widget-links',		require("@c/widgets/WidgetLinks.vue").default);
Vue.component('widget-text',		require("@c/widgets/WidgetText.vue").default);
Vue.component('widget-image',		require("@c/widgets/WidgetImage.vue").default);


var app = new Vue({
    el: '#app',
    data: {
        message: 'Hello Vue!'
    },

    mounted() {
        
        // Эта штука ждёт, пока срендерится всё приложение
        // аналог ready в jQuery
        this.$nextTick(function () {
            document.querySelector('body').classList.add('loaded');

            // Всё что связано с адаптивкой
            var r = responsive.init();

            enquire.register(r.matches.sm, {

                match : function() {

                    // Перемещаем кнопки из шапки во всплывающее меню
                    let target = document.querySelector(".mobile-nav");

                        target.appendChild(document.querySelector(".header__nav-list"));
                        target.appendChild(document.querySelector(".header__buttons"));
                        target.appendChild(document.querySelector(".header__lang-switch"));

                    // Перемещение списка категорий над списком статей
                    if (document.querySelector(".blog-arhive__row")) {
                        let blog = document.querySelector(".blog-arhive__row");
                        blog.insertBefore(document.querySelector(".js-rdom__blog-wlinks"), document.querySelector(".blog-arhive__row > :first-child"));
                    }

                },

                unmatch : function() {
                    // Перемещаем кнопки из всплывающего меню в шапку
                    document.querySelector(".header__nav-container").appendChild(document.querySelector(".header__nav-list"));
                    document.querySelector(".header__container").appendChild(document.querySelector(".header__buttons"));
                    document.querySelector(".header__container").appendChild(document.querySelector(".header__lang-switch"));

                    // Перемещение списка категорий в сайдбар
                    if (document.querySelector(".blog-arhive__row")) {
                        document.querySelector(".blog-archive__sidebar").insertBefore(document.querySelector(".js-rdom__blog-wlinks"), document.querySelector(".blog-archive__sidebar > :first-child"));
                    }
                }

            });

            mobileNav.init();
            popup.init();

        });
    }
});

// перенес в выше в $nextTick
// window.addEventListener('load', () => {
//     popup.init();
// });