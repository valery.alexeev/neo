export default function(option) {
    if (typeof window.wp_options != 'undefined' && typeof window.wp_options[option] != 'undefined') {
        return window.wp_options[option];
    } else {
        return false;
    }
}