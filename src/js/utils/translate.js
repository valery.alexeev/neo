export default function(string) {
    if (typeof window.wp_trans != 'undefined' && typeof window.wp_trans[string] != 'undefined') {
        return window.wp_trans[string];
    } else {
        console.log('Не переведено: ' + string);
        return string;
    }
}