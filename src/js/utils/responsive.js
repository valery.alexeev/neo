import findLastKey from "lodash-es/findLastKey";
import each from "lodash-es/each";

var responsive = {
    sizes: {
        xs: 0,
        sm: 576,
        md: 768,
        lg: 992,
        xl: 1260
    },
    matches: {
        xs: "screen and (max-width: 576px)",
        sm: "screen and (max-width: 768px)",
        md: "screen and (max-width: 992px)",
        lg: "screen and (max-width: 1260px)",
        xl: "screen and (min-width: 1261px)"
    },
    current: null,
    init: function(){

        setInterval(() => {
            this.detectCurrent();
        }, 100);

        return this;
    },
    detectCurrent: function(){
        var oldSize = this.current,
            newSize = findLastKey(this.sizes, function(s){return window.matchMedia(`screen and (min-width: ${s + 1}px)`).matches});

        this.current = newSize;
        each(this.sizes, (value, key) => {
            this[`is_${key}`] = this.current == key || value < this.sizes[this.current];
        });

        if (oldSize != newSize) {
            window.dispatchEvent( new CustomEvent('responsive_changed', {detail: {old: this.current, new: newSize, r: this}}) );
        }
    }
};

export default responsive;