export default function(page_id) {
    window.wp_pages = {};

    if (typeof window.wp_pages[page_id] != 'undefined') {
        return window.wp_pages[page_id];
    } else {
        var request = axios.get(window.env.WP_API_URL + page_id);

        request.then(data => {
            window.wp_pages[data.data.id] = data.data;
        });

        return request;
    }
}