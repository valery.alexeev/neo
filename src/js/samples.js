export default {
    load: function(value, repeat) {
        if (typeof window.env != 'undefined' && window.env.ENV == 'local') {
            let n = repeat || 1,
				r = this[`${value}`];
				
            if (n > 1) {
                for (let i = 1; i < n; i++) {
                    r = r.concat(this[`${value}`]);
                }
            }

            return r;
        } else {
            return null;
        }
    },
    widgetLinks: {
        'Все'					: '/blog',
        'Здоровье'				: '/blog',
        'Пресса о Школе'		: '/blog',
        'Вадим Зеланд о Школе'	: '/blog',
        'Ученые о методе'		: '/blog',
        'Прямое видение'		: '/blog',
        'Энергетика'			: 'https://ya.ru',
        'Психобиокомпьютер'		: '/blog.html'
    },
    blog: [
        {
            img: 'img/content/blog-1.png',
            title: 'Информационная социальная экология XXI века',
            date: "14 ноября",
            link: "./blog-single.html",
            text: 'Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor.',
            taxonomy: {
                title: "Здоровье",
                link: "/blog"
            }
        }, 
        {
            img: 'img/content/blog-2.png',
            title: 'Информационная социальная экология XXI века',
            date: "14 ноября",
            link: "./blog-single.html",
            text: 'Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor.',
            taxonomy: {
                title: "Ученые о методе",
                link: "/blog"
            }
        },
    ],
    card_tarife: [{
        title: 'Базовый пакет +',
        subtitle: 'Живое участие в Москве в мини-группе',
        content: [
            {item:'20 часов групповой работы' },
            {item:'Методический материал и рабочая тетрадь' },
            {item:'Индивидуальная консультация тренера' },
            {item:'Составление индивидуального плана занятий' },
            {item:'Все записи тренинга в видео-формате' },
        ],
        prices: {
            price: '1 290 ₽',
            price_note: 'сейчас',
            price_cross: '2 490₽',
            price_cross_note: 'с 24 ноября'
        },
        btn: {
            url: '#',
            title: 'Выбрать пакет'
        },
        places: 'Осталось всего 12 мест',
        type: 'popup',
        popup_inner: '<h2>заголовок 2</h2><p>Сюда через v-html добавляется код формы</p><ul><li>test 1</li><li>test 2</li></ul>',
    }],
    media: [
        {   
            img: {
                url: 'img/content/media-1.jpg'
            },
            link_type: 'video',
            link: 'https://www.youtube.com/embed/sjCeXGcN__g',
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'video',
            link: 'https://www.youtube.com/embed/16xGceArnz8?autoplay=1',
            img: {
                url: 'img/content/media-2.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'video',
            link: 'https://www.youtube.com/embed/sjCeXGcN__g',
            img: {
                url: 'img/content/media-3.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'video',
            link: 'https://www.youtube.com/embed/sjCeXGcN__g',
            img: {
                url: 'img/content/media-1.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'image',
            link: '',
            img: {
                url: 'img/content/media-2.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'image',
            link: '',
            img: {
                url: 'img/content/media-3.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'video',
            link: 'https://www.youtube.com/embed/sjCeXGcN__g',
            img: {
                url: 'img/content/test-image-small.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
        {
            link_type: 'image',
            link: '',
            img: {
                url: 'img/content/test-image-big.jpg'
            },
            title: 'Резервы мозга, резервы человечности?',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattisNulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis'
        },
    ],
    advantageText: [
        {
            img: {
                url: 'img/content/adv-1.svg'
            },
            title: 'Целительство',
            text: 'Восстановление органов, функций, энергии организма своего и других людей, а также всего живого',
            link: '#'
        },
        {
            img: {
                url: 'img/content/adv-2.svg'
            },
            title: 'Сверх-чувствительность',
            text: 'Чувствовать внутреннюю и внешнюю энергию и информацию, управлять этими процессами'
        },
        {
            img: {
                url: 'img/content/adv-3.svg'
            },
            title: 'Защита',
            text: 'Использование навыков для предотвращения негативного влияния'
        },
        {
            img: {
                url: 'img/content/adv-4.svg'
            },
            title: 'Видение',
            text: 'Видеть энергию, информацию, внутренние органы и другое — то есть то, что невозможно увидеть глазами'
        },
        {
            img: {
                url: 'img/content/adv-5.svg'
            },
            title: 'Сверхпамять',
            text: 'Оперировать большими объемами информации, уметь использовать различные виды памяти'
        },
        {
            img: {
                url: 'img/content/adv-6.svg'
            },
            title: 'Проектирование',
            text: 'Создавать, влиять и видеть варианты будущих событий,  реконструкция прошлых'
        },
    ],
    advantageBlock: [
        {
            img: {
                url: 'img/content/adv-n-1.svg'
            },
            title: '15 лет',
            text: 'успешной работы<br>школы',
            link: '#'
        },
        {
            img: {
                url: 'img/content/adv-n-2.svg'
            },
            title: '35 000',
            text: 'благодарных<br>учеников'
        },
        {
            img: {
                url: 'img/content/adv-n-3.svg'
            },
            title: '8 лет',
            text: 'исследований в институте мозга человека'
        },
        {
            img: {
                url: 'img/content/adv-n-4.svg'
            },
            title: '13 000',
            text: 'консультаций<br>и проведенных диагностик'
        },
    ],
    advantageNumber: [
        {
            title: 'Feature 1',
            text: 'Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis'
        },
        {
            title: 'Feature 2',
            text: 'Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis'
        },
        {
            title: 'Feature 3',
            text: 'Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis'
        },
        {
            title: 'Feature 4',
            text: 'Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis'
        },
        {
            title: 'Feature 5',
            text: 'Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis'
        },
        {
            title: 'Feature 6',
            text: 'Vestibulum id ligula porta felis euismod semper. Nullam quis risus eget urna mollis'
        },
    ],
    feedback: [
        {   
            type: 'text',
            title: 'Тип отзыва только текст',
            popup_inner : '<h4>Тип отзыва только ТЕКСТ</h4><p>Контент попапа2</p>',
            text: 'Появилось большое желание что-то дать миру и окружающим, передать свой опыт Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum… Praesent commodo cursus magna, vel scelerisque nisl consectetur et.'
        },
        {   
            type: 'video',
            video: 'https://www.youtube.com/embed/16xGceArnz8?autoplay=1',
            photo: 'img/content/card-feedback1.jpg',
            title: 'Тип отзыва видео + текст',
            popup_inner : '<h4>Тип отзыва видео + текст</h4><p>Контент попапа1</p>',
            text: 'Появилось большое желание что-то дать миру и окружающим, передать свой опыт Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum… Praesent commodo cursus magna, vel scelerisque nisl consectetur et.'
        },
        {    
            type: 'photo',
            video: 'https://www.youtube.com/embed/16xGceArnz8?autoplay=1',
            photo: 'img/content/card-feedback1.jpg',
            title: 'Тип отзыва только фото',
            popup_inner : '',
            text: 'Появилось большое желание что-то дать миру и окружающим, передать свой опыт Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum… Praesent commodo cursus magna, vel scelerisque nisl consectetur et.'
        },
        {   
            type: 'video',
            video: 'https://www.youtube.com/embed/16xGceArnz8?autoplay=1',
            photo: 'img/content/card-feedback1.jpg',
            title: 'Тип отзыва только видео',
            popup_inner : '',
            text: 'Появилось большое желание что-то дать миру и окружающим, передать свой опыт Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Sed posuere consectetur est at lobortis. Curabitur blandit tempus porttitor. Maecenas faucibus mollis interdum… Praesent commodo cursus magna, vel scelerisque nisl consectetur et.'
        },
    ],
    coach: [
        {        
            img: 'img/content/card-coach1.jpg',
            name: 'Владимир Бронников',
            position: 'Специалист по медитациям',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis consectetur purus sit amet fermentum.',
            url: "#" 
        },
        {        
            img: 'img/content/card-coach1.jpg',
            name: 'Владимир Бронников',
            position: 'Специалист по медитациям',
            text: 'Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Cras mattis consectetur purus sit amet fermentum.',
            url: "#" 
        },
        {        
            img: 'img/content/card-coach1.jpg',
            name: 'Владимир Бронников',
            position: 'Специалист по медитациям',
            text: 'мало текста',
            url: "#" 
        },
        {        
            img: 'img/content/card-coach1.jpg',
            name: 'Владимир Бронников',
            position: 'Специалист по медитациям',
            text: 'мало текста',
            url: "#" 
        },
    ],
    timeline: [
        {
            date: '2012 год',
            title: 'Tortor Adipiscing Ipsum Porta',
            text: 'Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Nullam id dolor id nibh ultricies vehicula ut id elit. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum.'
        },
        {    
            img: 'img/content/card-timelone1.jpg',
            date: '2012 год',
            title: 'Tortor Adipiscing Ipsum Porta',
            text: 'Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Nullam id dolor id nibh ultricies vehicula ut id elit. Donec ullamcorper nulla non metus auctor fringilla.'
        },
        {
            date: '2012 год',
            title: 'Tortor Adipiscing Ipsum Porta',
            text: '<p>Donec ullamcorper nulla non metus auctor fringilla. Sed posuere consectetur est at lobortis. Nullam id dolor id nibh ultricies vehicula ut id elit. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum.</p><p>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Sed posuere consectetur est at lobortis. Donec id elit non mi porta gravida </p>'
        }
    ],
    points: [
        {
            city:       'Москва',
            address:    'Дмитровское шоссе, д.39, корп.4, офис. 312',
            position:   {lat: 55.779495, lng: 37.518091},
            images:     ['img/content/card-coach1.jpg','img/content/card-timelone1.jpg']
        },
        {
            city:       'Москва',
            address:    'Москва, метро Алексеевская (5 минут пешком), 3-я Мытищинская улица, дом 3, строение 1, этаж 8. Все встречи и тренинги только по предварительной записи в назначенное время',
            position:   {lat: 55.779495, lng: 37.538091}
        },
        {
            city:       'Казань',
            address:    'Адрес в казани',
            position:   {lat: 55.759495, lng: 37.528091},
            images:     ['img/content/card-coach1.jpg','img/content/card-coach1.jpg']
        },
        {
            city:       'Санкт-Петербург',
            address:    'Адресс в Санкт-Петербурге',
            position:   {lat: 55.779495, lng: 38.538091}
        },
    ],
    events: [
        {
            date:       '1 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Москва',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-1.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '2 ноября',
            type:       {offline: false, online: true, free: false},
            city:       '',
            price:      {basic: '14990 ₽', discount: ''},
            image:      'img/content/card-event-2.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '3 ноября',
            type:       {offline: true, online: true, free: false},
            city:       'Москва',
            price:      {basic: '2490 ₽', discount: '1290 ₽'},
            image:      'img/content/card-event-3.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Олег Тиньков'],
            link:       '#'
        },
        {
            date:       '4 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Москва',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-4.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '5 ноября',
            type:       {offline: false, online: true, free: false},
            city:       '',
            price:      {basic: '14990 ₽', discount: ''},
            image:      'img/content/card-event-5.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#',
            date_t:     1
        },
        {
            date:       '6 ноября',
            type:       {offline: true, online: true, free: false},
            city:       'Москва',
            price:      {basic: '2490 ₽', discount: '1290 ₽'},
            image:      'img/content/card-event-1.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Олег Тиньков'],
            link:       '#',
            date_t:     2
        },
        {
            date:       '7 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Москва',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-1.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '8 ноября',
            type:       {offline: false, online: true, free: false},
            city:       '',
            price:      {basic: '14990 ₽', discount: ''},
            image:      'img/content/card-event-2.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '9 ноября',
            type:       {offline: true, online: true, free: false},
            city:       'Москва',
            price:      {basic: '2490 ₽', discount: '1290 ₽'},
            image:      'img/content/card-event-3.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Олег Тиньков'],
            link:       '#'
        },
        {
            date:       '10 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Москва',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-1.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '11 ноября',
            type:       {offline: false, online: true, free: false},
            city:       '',
            price:      {basic: '14990 ₽', discount: ''},
            image:      'img/content/card-event-2.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '12 ноября',
            type:       {offline: true, online: true, free: false},
            city:       'Санкт-Петербург',
            price:      {basic: '2490 ₽', discount: '1290 ₽'},
            image:      'img/content/card-event-3.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Олег Тиньков'],
            link:       '#'
        },
        {
            date:       '13 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Москва',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-4.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '14 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Санкт-Петербург',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-1.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '15 ноября',
            type:       {offline: false, online: true, free: false},
            city:       '',
            price:      {basic: '14990 ₽', discount: ''},
            image:      'img/content/card-event-2.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            date:       '16 ноября',
            type:       {offline: true, online: true, free: false},
            city:       'Москва',
            price:      {basic: '2490 ₽', discount: '1290 ₽'},
            image:      'img/content/card-event-1.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Олег Тиньков'],
            link:       '#'
        },
        {
            date:       '17 ноября',
            type:       {offline: true, online: false, free: true},
            city:       'Санкт-Петербург',
            price:      {basic: '', discount: ''},
            image:      'img/content/card-event-4.jpg',
            title:      'Оффлайн мастер-класс «Открытие восьмой чакры»',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
    ],
    consultation: [
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-1.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-2.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Анна Бронникова'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-3.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Лейла Нерсесова', 'Анна Бронникова'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-4.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Анна Бронникова'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-5.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Лейла Нерсесова'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-2.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Роман Яковленко'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-3.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Лейла Нерсесова', 'Олеся Невская'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-4.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Роман Яковленко'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-5.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Олеся Невская'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-2.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Владимир Бронников', 'Лейла Нерсесова'],
            link:       '#'
        },
        {
            price:      {basic: '14990'},
            image:      'img/content/card-cons-3.jpg',
            title:      'Неизлечимо больная спина. Остеохондроз.',
            text:       'I ступень. Развитие сверхчувствительного восприятия. Neoздоровье и Neoэнергетика',
            speakers:   ['Роман Яковленко', 'Анна Бронникова'],
            link:       '#'
        },
    ],
    notes: [
        {
            img: {
                sizes:  {
                    medium: 'img/content/card-rnote-1.jpg'
                }
            },
            date: '1 марта',
            title: 'Информационная социальная',
            text: 'Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor.',
            link: '#'
        },
        {
            img: {
                sizes:  {
                    medium: 'img/content/test-image-small.jpg'
                }
            },
            date: '2 марта',
            title: 'Информационная социальная',
            text: 'Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor.',
            link: '#'
        },
        {
            img: {
                sizes:  {
                    medium: 'img/content/test-image-big.jpg'
                }
            },
            date: '3 марта',
            title: 'Информационная социальная',
            text: 'Curabitur blandit tempus porttitor. Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla. Curabitur blandit tempus porttitor.',
            link: '#'
        }
    ]
}