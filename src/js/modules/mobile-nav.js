const bodyScrollLock = require('body-scroll-lock');
const disableBodyScroll = bodyScrollLock.disableBodyScroll;
const enableBodyScroll = bodyScrollLock.enableBodyScroll;

let nav = {
    classes: {
        menuButton: "header__mobile__nav",
        menuCloseButton: "mobile-nav__close",
        menu: "mobile-nav",
        overlay: "overlay",
        menuOpened: "mobile-nav--opened",
        overlayOpened: "overlay--opened"
    },

    menu: null,
    overlay: null,

    close: function(){
        enableBodyScroll(this.menu);
        this.menu.classList.remove(this.classes.menuOpened);
        this.overlay.classList.remove(this.classes.overlayOpened);
    },
    open: function(){
        disableBodyScroll(this.menu);
        this.menu.classList.add(this.classes.menuOpened);
        this.overlay.classList.add(this.classes.overlayOpened);
    },

    init: function(){
        this.menu = document.querySelector("." + this.classes.menu);
        this.overlay = document.querySelector("." + this.classes.overlay);

        document.querySelector("." + this.classes.menuButton).addEventListener("click", () => {
            this.open();
        });

        document.querySelector("." + this.classes.menuCloseButton).addEventListener("click", () => {
            this.close();
        });

        this.overlay.addEventListener("touchstart", () => {
            this.close();
        });

        this.overlay.addEventListener("click", () => {
            this.close();
        });
    },
};

export default nav;