import tingle from "tingle.js";

export default {
	wrap: document.querySelector('.page-content'), // для добавления класса с blur эффектом 
	init : function () {
		if (!document.querySelector('.js-tingle-modal')) return false;


		let popupLinks = document.querySelectorAll('.js-tingle-modal');

		[].forEach.call(popupLinks, (item) => {
			item.addEventListener('click', (event) => {
				event.preventDefault();

				if (item.matches('.js-tingle-modal--image')) {
					this.runImagePopup(item);
				} else if (item.matches('.js-tingle-modal--video')) {
					this.runVideoPipup(item);
				} else if (item.matches('.js-tingle-modal--text')) {
					this.runTextPopup(item);
				} else if (item.matches('.js-tingle-modal--template')) {
					this.runTemplatePopup(item);
				}
			}, true);
		});

		// document.addEventListener('click', (event) => {
		// 	// console.log(event.target);
		// 	// event.preventDefault();
		// // 	if (event.target.matches('.js-tingle-modal--text')) {
		// // 		// Копирует только контент
		// // 		this.runTextPopup(event);
		// // 	} else if (event.target.matches('.js-tingle-modal--video')) {
		// // 		// для видео
		// // 		this.runVideoPipup(event);
		// // 	} else if (event.target.matches('.js-tingle-modal--template')) {
		// // 		// Сохраняет весь узел копируя id и не выставляя display block, по сути не нужное
		// // 		this.runTemplatePopup(event);
		// // 	} else if (event.target.matches('.js-tingle-modal--image')) {
		// // 		// для изображений
		// // 		console.log(1);
		// // 		// this.runImagePopup(event);
		// // 	}
		// }, true);
	},
	runTextPopup(item) {
		let link = item.getAttribute('href');
		let content = document.querySelector(link);
		let modalOptions = {
			onClose: () => {
				this.wrap.classList.remove('tingle-content-wrapper');
				modalText.destroy();
			}
		};
		if (event.target.classList.contains('js-tingle-modal--small')) {
			modalOptions['cssClass'].push('tingle-modal--small');
		}


		this.wrap.classList.add('tingle-content-wrapper');
		let modalText = new tingle.modal(modalOptions);

		if (content) {
			modalText.setContent(content.innerHTML);
		} else {
			modalText.setContent('Контент не найден');
		}
		modalText.open();
	},
	runImagePopup(item) {
		let link = item.getAttribute('href');
		function createImage(url) { 
			let image = document.createElement('img');

			image.setAttribute('src', url);

			return image;
		}

		this.wrap.classList.add('tingle-content-wrapper');
		let modalImage = new tingle.modal({
			cssClass: ['tingle-modal--image'],
			onClose: () => {
				this.wrap.classList.remove('tingle-content-wrapper');
				modalImage.destroy();
			}
		});
		modalImage.setContent(createImage(link));
		modalImage.open();
	},
	runVideoPipup(item) {
		let link = this.parseVideoLink(item.href);
		function createIframe(url) { // создаем iframe для видео
			let iframe = document.createElement('iframe');

			iframe.setAttribute('src', url);
			iframe.setAttribute('allowfullscreen', '');
			iframe.setAttribute('frameborder', '0');
			
			return iframe;
		}

		this.wrap.classList.add('tingle-content-wrapper');
		let modalVideo = new tingle.modal({
			cssClass: ['tingle-modal--video'],
			onClose: () => {
				this.wrap.classList.remove('tingle-content-wrapper');
				modalVideo.destroy();
			}
		});
		modalVideo.setContent(createIframe(link));
		modalVideo.open();
	},
	runTemplatePopup(item) {
		let link = item.getAttribute('href');
		let content = document.querySelector(link);
		let contentClone = content.cloneNode(true);
		let modalOptions = {
			closeLabel: 'Закрыть',
			cssClass: ['tingle-modal--template'],
			onClose: () => {
				this.wrap.classList.remove('tingle-content-wrapper');
				modalTemplate.destroy();
			}
		};
		if (event.target.classList.contains('js-tingle-modal--small')) {
			modalOptions['cssClass'].push('tingle-modal--small');
		}

		this.wrap.classList.add('tingle-content-wrapper');
		let modalTemplate = new tingle.modal(modalOptions);

		if (contentClone) {
			modalTemplate.setContent(contentClone.innerHTML);
		} else {
			modalTemplate.setContent('Контент не найден');
		}
		modalTemplate.open();
	},
	regexes: {
		youtube: /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/,
		vimeo: /(?:vimeo)\.com.*(?:videos|video|channels|)\/([\d]+)/i
	},

	parseVideoLink: function(url) {
		let result = false;

		// Check YouTube
		let match = url.match(this.regexes.youtube);

		if ( match && match[7].length == 11 ){
			result = "//youtube.com/embed/" + match[7];
		} else {

			// Check Vimeo link
			let match2 = url.match(this.regexes.vimeo);
			if (match2 && match2[1] ) {
				result = "//player.vimeo.com/video/" + match2[1];
			}
		}

		return result;
	}
}